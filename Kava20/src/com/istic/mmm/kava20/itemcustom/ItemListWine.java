package com.istic.mmm.kava20.itemcustom;

import android.net.Uri;

public class ItemListWine {
	 
    private int id;
	private Uri wineImageSrc;
    private String wineYear;
    private String wineName;
    private float wineRating;
    private int wineNumber;
 
    public ItemListWine(int id, Uri wineImageSrc, String wineName, String wineYear, float wineRating, int wineNumber) {
        super();
        this.id = id;
        this.wineImageSrc = wineImageSrc;
        this.wineYear = wineYear;
        this.wineName = wineName;
        this.wineRating = wineRating;
        this.wineNumber = wineNumber;
    }
    // getters and setters...   

	public int getWineNumber() {
		return wineNumber;
	}

	public void setWineNumber(int wineNumber) {
		this.wineNumber = wineNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Uri getWineImageSrc() {
		return wineImageSrc;
	}

	public void setWineImageSrc(Uri wineImageSrc) {
		this.wineImageSrc = wineImageSrc;
	}

	public String getWineYear() {
		return wineYear;
	}

	public void setWineYear(String wineYear) {
		this.wineYear = wineYear;
	}

	public String getWineName() {
		return wineName;
	}

	public void setWineName(String wineName) {
		this.wineName = wineName;
	}

	public float getWineRating() {
		return wineRating;
	}

	public void setWineRating(float wineRating) {
		this.wineRating = wineRating;
	}

    
}

