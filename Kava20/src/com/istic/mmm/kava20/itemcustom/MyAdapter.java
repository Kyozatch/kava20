package com.istic.mmm.kava20.itemcustom;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.istic.mmm.kava20.R;

public class MyAdapter extends BaseAdapter implements Filterable{



	static class ViewHolder {
		ImageView img;
		RatingBar rBar;
		TextView textName;
		TextView textYear;
		TextView textNumber;
	}

	private final Context context;
	private itemFilter itFilter;
	private ArrayList<ItemListWine> winesArrayList;
	private ArrayList<ItemListWine> winesFilteredArrayList;

	public MyAdapter(Context context, ArrayList<ItemListWine> winesArrayList) {
		super();
		this.context = context;
		this.winesArrayList = winesArrayList;
		this.winesFilteredArrayList = winesArrayList;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })	
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		if(rowView == null){
			// 1. Create inflater 
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// 2. Get rowView from inflater
			rowView = inflater.inflate(R.layout.custom_rowine, parent, false);

			// 3. Get the two text view from the rowView
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.img = (ImageView) rowView.findViewById(R.id.WineImage);
			viewHolder.rBar = (RatingBar) rowView.findViewById(R.id.WineRating);
			viewHolder.textName = (TextView) rowView.findViewById(R.id.WineName);
			viewHolder.textYear = (TextView) rowView.findViewById(R.id.WineYear);
			viewHolder.textNumber = (TextView) rowView.findViewById(R.id.WineNumber);
			rowView.setTag(viewHolder); 
		}

		// 4. Set values
		ViewHolder holder = (ViewHolder) rowView.getTag();

		ItemListWine filteredItem = winesFilteredArrayList.get(position);

		Uri myUri = filteredItem.getWineImageSrc();

		Options options = new BitmapFactory.Options ();
		options.inJustDecodeBounds = true;
		options.inSampleSize = 20;
		options.outHeight = 100;
		options.outWidth = 150;
		options.inPreferQualityOverSpeed = false;
		options.inDither = false;
		options.inJustDecodeBounds = false;
		
		try{
			holder.img.setImageBitmap(BitmapFactory.decodeFile(getRealPathFromURI(context, myUri), options));
		}catch(NullPointerException e){
			holder.img.setImageURI(myUri);
		}
		
		holder.rBar.setRating(filteredItem.getWineRating());
		holder.textName.setText(filteredItem.getWineName());
		holder.textYear.setText(filteredItem.getWineYear());
		holder.textNumber.setText(String.valueOf(filteredItem.getWineNumber()));

		// 5. return rowView
		return rowView;
	}

	public String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try { 
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	@Override
	public int getCount() {
		return winesFilteredArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return winesFilteredArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position; //winesArrayList.get(position).getWineId();
	}

	public void clear(){
		this.winesArrayList.clear();
		this.notifyDataSetChanged();
	}

	public void addItem(ItemListWine Item){
		winesArrayList.add(Item);
		notifyDataSetChanged();
	}

	public void removeItem(int arg1){
		winesArrayList.remove(arg1);
		notifyDataSetChanged();
	}

	@Override
	public Filter getFilter() {
		if (itFilter == null) {
			itFilter = new itemFilter();
		}

		return itFilter;
	}

	private class itemFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults filterResults = new FilterResults();
			if (constraint!=null && constraint.length()>0) {
				ArrayList<ItemListWine> tempList = new ArrayList<ItemListWine>();

				// search content in friend list
				for (ItemListWine it : winesArrayList) {
					if (it.getWineName().toLowerCase(Locale.FRANCE).contains(constraint.toString().toLowerCase())) {
						tempList.add(it);
					}
				}
				filterResults.count = tempList.size();
				filterResults.values = tempList;
				//				for (ItemListWine it : tempList) {
				//					System.out.println("if "+ it.getWineName());
				//				}
			} else {
				filterResults.count = winesArrayList.size();
				filterResults.values = winesArrayList;
				//				for (ItemListWine it : winesArrayList) {
				//					System.out.println("else "+ it.getWineName());
				//				}
			}

			return filterResults;
		}

		/**
		 * Notify about filtered list to ui
		 * @param constraint text
		 * @param results filtered result
		 */
		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			winesFilteredArrayList = (ArrayList<ItemListWine>) results.values;
			for (ItemListWine it : winesArrayList) {
				System.out.println("publiche "+ it.getWineName());
			}
			notifyDataSetChanged();
		}

	}
}