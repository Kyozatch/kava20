package com.istic.mmm.kava20;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.istic.mmm.kava20.dragndrop.ListViewDragDrop;
import com.istic.mmm.kava20.itemcustom.ItemListWine;
import com.istic.mmm.kava20.itemcustom.MyAdapter;


public class MainActivity extends Activity {

	private ItemListWine itemListWineSelected;
	private ImageView imageDrag;
	private Context context;
	private ListViewDragDrop lv;
	private MyAdapter adapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.index);    
		
        imageDrag = (ImageView) findViewById(R.id.WineImage);
        
        // 1. pass context and data to the custom adapter
        adapter = new MyAdapter(this, generateData());
 
        // 2. Get ListView from activity_main.xml
        lv = (ListViewDragDrop) findViewById(R.id.listWine);
        
        // 3. setListAdapter
        lv.setAdapter(adapter);
        
		//Set selected Listener to know what item must be drag
		lv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				/**
				 * retrieve selected item from adapterview
				 */
				itemListWineSelected = (ItemListWine) arg0.getItemAtPosition(arg2);
			//	imageDrag.setImageDrawable(context.getResources().getDrawable(itemListWineSelected.getWineImageSrc()));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//Set an touch listener to know what is the position when item are move out of the listview 
		lv.setOnItemMoveListener(new  OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				RelativeLayout.LayoutParams layout = (RelativeLayout.LayoutParams) imageDrag.getLayoutParams();
				imageDrag.setVisibility(ImageView.VISIBLE);
				
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					imageDrag.bringToFront();
					return true;
				}
				
				if (event.getAction()==MotionEvent.ACTION_MOVE) {
					layout.leftMargin = (int) event.getX() - imageDrag.getWidth()/2;    		
					layout.topMargin = (int) event.getY() - imageDrag.getHeight()/2;
				}
				
				if (event.getAction()==MotionEvent.ACTION_UP) {
					imageDrag.setVisibility(View.GONE);
				}
				
				imageDrag.setLayoutParams(layout);
				
				return true;
			}

		});
		
		 //Listener to know if the item is droped out of this origin ListView 
		//lv.setOnItemUpOutListener(mOnItemUpOutListener);
   
//		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
//			
//			@Override
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//				adapter.removeItem(arg2);
//				return false;
//			}
//
//		});
		
    }
 
    private ArrayList<ItemListWine> generateData(){
        ArrayList<ItemListWine> ItemListWines = new ArrayList<ItemListWine>();
//        ItemListWines.add(new ItemListWine(R.drawable.ic_launcher, "Chateau Latourd","1995", (float)1.5));
//        ItemListWines.add(new ItemListWine(R.drawable.ic_launcher, "Champagne","2011", (float)2.0));
//        ItemListWines.add(new ItemListWine(R.drawable.ic_launcher, "Saint �milion","1970", (float)4.5));
 
        return ItemListWines;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds ItemListWines to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem Item) {
        // Handle action bar ItemListWine clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = Item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(Item);
    }
}
