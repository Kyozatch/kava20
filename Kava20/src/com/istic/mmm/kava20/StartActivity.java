package com.istic.mmm.kava20;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.istic.mmm.kava20.databases.VinDataSource;
import com.istic.mmm.kava20.dragndrop.PassItem;
import com.istic.mmm.kava20.itemcustom.ItemListWine;
import com.istic.mmm.kava20.itemcustom.MyAdapter;
import com.istic.mmm.kava20.model.Vin;


public class StartActivity extends Activity implements OnQueryTextListener{

	public final static String ID_WINE = "com.istic.mmm.kava20.ID_WINE";

	private LinearLayout layout;
	private ListView lv;
	private MyAdapter adapter;
	private ArrayList<ItemListWine> listItem;
	private LinearLayout layoutAll;
	private LinearLayout layoutMaturation;
	private LinearLayout layoutToDrink;
	private LinearLayout layoutToDrinkFast;
	private LinearLayout layoutDrunk;
	private VinDataSource DBconnexion;
	private Button selectedButton; 
	private ItemListWine currentDragItem;
	private String idWine;
	private SearchView searchView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index);

		//instanciate listitem
		listItem = new ArrayList<ItemListWine>();

		// Get widgets from activity_main.xml
		layout = (LinearLayout)findViewById(R.id.layoutlist);	
		lv = (ListView) findViewById(R.id.listWine);
		layoutAll = (LinearLayout)findViewById(R.id.layoutall);
		layoutMaturation = (LinearLayout)findViewById(R.id.layoutmaturation);
		layoutToDrink = (LinearLayout)findViewById(R.id.layouttodrink);
		layoutToDrinkFast = (LinearLayout)findViewById(R.id.layouttodrinkfast);
		layoutDrunk = (LinearLayout)findViewById(R.id.layoutdrunk);
		selectedButton = (Button)findViewById(R.id.buttonall);

		//set draglistener on buttons
		layoutAll.setOnDragListener(myOnDragListener);
		layoutMaturation.setOnDragListener(myOnDragListener);
		layoutToDrink.setOnDragListener(myOnDragListener);
		layoutToDrinkFast.setOnDragListener(myOnDragListener);
		layoutDrunk.setOnDragListener(myOnDragListener);

		//fill listitem 
		generateData();

		//fill listview
		adapter = new MyAdapter(this, listItem);
		lv.setAdapter(adapter);

		//set parameters and listeners to listview
		lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		lv.setTextFilterEnabled(true);
		lv.setOnItemClickListener(listOnItemClickListener);
		lv.setOnItemLongClickListener(myOnItemLongClickListener);

	}



	OnDragListener myOnDragListener = new OnDragListener() {

		@SuppressLint("NewApi")
		@Override
		public boolean onDrag(View v, DragEvent event) {
			LinearLayout newParent = (LinearLayout)v;
			Button btn;
			int etat;
			String etatText;

			if(newParent.equals(layoutAll))
			{
				btn = (Button) findViewById(R.id.buttonall);
				etat = 0;
				etatText = "non class�";
			}
			else if(newParent.equals(layoutMaturation)) 
			{
				btn = (Button) findViewById(R.id.buttonmaturation);
				etat = 1;
				etatText = "maturation";
			}
			else if(newParent.equals(layoutToDrink)) 
			{
				btn = (Button) findViewById(R.id.buttontodrink);
				etat = 2;
				etatText = "� boire";
			}
			else if(newParent.equals(layoutToDrinkFast)) 
			{
				btn = (Button) findViewById(R.id.buttontodrinkfast);
				etat = 3;
				etatText = "� boire vite";
			}
			else if(newParent.equals(layoutDrunk)) 
			{
				btn = (Button) findViewById(R.id.buttondrunk);
				etat = 4;
				etatText = "bu";
			}
			else {
				btn = new Button(null);
				etat = -1;
				etatText = "d�posez l'item sur le menu vertical � gauche";
			}


			switch (event.getAction()) {
			case DragEvent.ACTION_DRAG_STARTED:
				break; 

			case DragEvent.ACTION_DRAG_ENTERED:
				btn.setBackgroundResource(R.color.selectbutton);
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				if (!btn.equals(selectedButton)){
					btn.setBackgroundResource(R.color.deselectbutton);
				}
				break; 

			case DragEvent.ACTION_DROP:

				if (etat > 0 && etat < 4){
					DBconnexion = new VinDataSource(v.getContext());
					DBconnexion.open();

					int itemID = currentDragItem.getId();
					Vin vin = DBconnexion.getVinById(itemID);
					vin.set_etat(etat);

					DBconnexion.updateVin(itemID,vin);
					selectedButton.callOnClick();
					DBconnexion.close();
					etatText = "Le vin " + vin.get_provenance().get_aoc() + " est en " + etatText;
				}
				else if (etat==4){
					DBconnexion = new VinDataSource(v.getContext());
					DBconnexion.open();

					int itemID = currentDragItem.getId();
					Vin vin = DBconnexion.getVinById(itemID);
					int nbVin = vin.get_nbBouteilles();
					int nbVinBus = vin.get_nbBouteilleBues();

					if (nbVin > 0){
						vin.set_nbBouteilles(nbVin-1);
						vin.set_nbBouteilleBues(nbVinBus+1);
					}
					DBconnexion.updateVin(itemID,vin);
					selectedButton.callOnClick();
					DBconnexion.close();
					etatText = "Le vin " + vin.get_provenance().get_aoc() + " est en " + etatText;
				}

				if (!btn.equals(selectedButton)){
					btn.setBackgroundResource(R.color.deselectbutton);
				}
				Toast.makeText(StartActivity.this, etatText, Toast.LENGTH_SHORT).show();
				break;

			case DragEvent.ACTION_DRAG_ENDED:
			default:
				break;    
			}
			return true;
		}
	};

	OnItemClickListener listOnItemClickListener = new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			ItemListWine selectedItem = (ItemListWine)(parent.getItemAtPosition(position));
			idWine = String.valueOf(selectedItem.getId());
			LaunchPVActivity(view);
		}
	};

	OnItemLongClickListener myOnItemLongClickListener = new OnItemLongClickListener(){

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			ItemListWine selectedItem = (ItemListWine)(parent.getItemAtPosition(position));
			PassItem passObj = new PassItem(view, selectedItem);
			setCurrentDragItem(selectedItem);

			ClipData data = ClipData.newPlainText("", "");
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(data, shadowBuilder, passObj, 0);

			return true;
		}

	};

	private void setCurrentDragItem (ItemListWine item){
		this.currentDragItem = item;
	}

	private void generateData() {

		DBconnexion = new VinDataSource(this);
		DBconnexion.open();
		fillListItem(DBconnexion.getAllVins(), false);
		DBconnexion.close();
	}

	public void fillListItem(List<Vin> l, boolean drunk){
		listItem.clear();

		Iterator<Vin> iterator = l.iterator();
		int nbBottle;
		Vin v;
		while (iterator.hasNext()) {
			v = iterator.next();

			if (drunk){
				nbBottle = v.get_nbBouteilleBues();
			}
			else {
				nbBottle = v.get_nbBouteilles();
			}

			try{
				listItem.add(new ItemListWine(v.get_idVin(),Uri.parse(v.get_photoLien()),v.get_provenance().get_aoc(), String.valueOf(v.get_annee()),v.get_note(), nbBottle)); 

			}
			catch(Exception e){
				listItem.add(new ItemListWine(v.get_idVin(),Uri.parse("android.resource://" + getPackageName() + "/" + R.drawable.ic_launcher),v.get_provenance().get_aoc(), String.valueOf(v.get_annee()), v.get_note(), nbBottle)); 
			}
		}
		adapter = new MyAdapter(this, listItem);
		lv.setAdapter(adapter);
	}

	public void colorSelectedButton(){
		findViewById(R.id.buttonall).setBackgroundResource(R.color.deselectbutton);
		findViewById(R.id.buttonmaturation).setBackgroundResource(R.color.deselectbutton);
		findViewById(R.id.buttontodrink).setBackgroundResource(R.color.deselectbutton);
		findViewById(R.id.buttontodrinkfast).setBackgroundResource(R.color.deselectbutton);
		findViewById(R.id.buttondrunk).setBackgroundResource(R.color.deselectbutton);
		selectedButton.setBackgroundResource(R.color.selectbutton);
	}

	public void LaunchPVActivity(View view) {

		Intent intent = new Intent(view.getContext(), ProfilVinActivity.class);
		intent.putExtra(ID_WINE, idWine);
		idWine="";
		startActivity(intent);
	}

	public void OnClickBtnAll (View view) {

		DBconnexion = new VinDataSource(this);
		DBconnexion.open();

		fillListItem(DBconnexion.getAllVins(), false);
		adapter.notifyDataSetChanged();
		selectedButton = (Button) findViewById(R.id.buttonall);
		colorSelectedButton();

		DBconnexion.close();
	}

	public void OnClickBtnMaturation (View view) {
		DBconnexion = new VinDataSource(this);
		DBconnexion.open();

		fillListItem(DBconnexion.getListVinByEtat(1), false);
		adapter.notifyDataSetChanged();
		selectedButton = (Button) findViewById(R.id.buttonmaturation);
		colorSelectedButton();

		DBconnexion.close();
	}

	public void OnClickBtnToDrink (View view) {
		DBconnexion = new VinDataSource(this);
		DBconnexion.open();

		fillListItem(DBconnexion.getListVinByEtat(2), false);
		adapter.notifyDataSetChanged();
		selectedButton = (Button) findViewById(R.id.buttontodrink);
		colorSelectedButton();

		DBconnexion.close();
	}

	public void OnClickBtnToDrinkFast (View view) {
		DBconnexion = new VinDataSource(this);
		DBconnexion.open();

		fillListItem(DBconnexion.getListVinByEtat(3), false);
		adapter.notifyDataSetChanged();
		selectedButton = (Button) findViewById(R.id.buttontodrinkfast);
		colorSelectedButton();

		DBconnexion.close();
	}

	public void OnClickBtnDrunk (View view) {
		DBconnexion = new VinDataSource(this);
		DBconnexion.open();

		fillListItem(DBconnexion.getListVinsBus(true), true);
		adapter.notifyDataSetChanged();
		selectedButton = (Button) findViewById(R.id.buttondrunk);
		colorSelectedButton();

		DBconnexion.close();
	}

	public void LaunchStatsActivity(View view){
		Intent intent = new Intent(view.getContext(), DiagramEtatVinActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds ItemListWines to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		searchView = (SearchView) menu.findItem(R.id.action_settings).getActionView();


		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setSubmitButtonEnabled(true);
		searchView.setOnQueryTextListener(this);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem Item) {
		// Handle action bar ItemListWine clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = Item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(Item);
	}

	@Override
	public boolean onQueryTextChange(String newText)
	{
		adapter.getFilter().filter(newText);
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		searchView.clearFocus();	
		return true;
	}

}
