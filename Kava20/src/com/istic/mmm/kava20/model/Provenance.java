package com.istic.mmm.kava20.model;

public class Provenance {
	private int _idProvenance;
	private String _aoc;
	private String _domaine;
	private String _region;
	private String _cepage;
	private String _terroir;
	
	public int get_idProvenance() {
		return _idProvenance;
	}
	public void set_idProvenance(int _idProvenance) {
		this._idProvenance = _idProvenance;
	}
	public String get_aoc() {
		return _aoc;
	}
	public void set_aoc(String _aoc) {
		this._aoc = _aoc;
	}
	public String get_domaine() {
		return _domaine;
	}
	public void set_domaine(String _domaine) {
		this._domaine = _domaine;
	}
	public String get_region() {
		return _region;
	}
	public void set_region(String _region) {
		this._region = _region;
	}
	public String get_cepage() {
		return _cepage;
	}
	public void set_cepage(String _cepage) {
		this._cepage = _cepage;
	}
	public String get_terroir() {
		return _terroir;
	}
	public void set_terroir(String _terroir) {
		this._terroir = _terroir;
	}
}
