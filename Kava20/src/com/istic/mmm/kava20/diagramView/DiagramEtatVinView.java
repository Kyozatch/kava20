/* ===========================================================
 * AFreeChart : a free chart library for Android(tm) platform.
 *              (based on JFreeChart and JCommon)
 * ===========================================================
 *
 * (C) Copyright 2010, by ICOMSYSTECH Co.,Ltd.
 * (C) Copyright 2000-2008, by Object Refinery Limited and Contributors.
 *
 * Project Info:
 *    AFreeChart: http://code.google.com/p/afreechart/
 *    JFreeChart: http://www.jfree.org/jfreechart/index.html
 *    JCommon   : http://www.jfree.org/jcommon/index.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * [Android is a trademark of Google Inc.]
 *
 * -----------------
 * PieChartDemo01View.java
 * -----------------
 * (C) Copyright 2010, 2011, by ICOMSYSTECH Co.,Ltd.
 *
 * Original Author:  Niwano Masayoshi (for ICOMSYSTECH Co.,Ltd);
 * Contributor(s):   -;
 *
 * Changes
 * -------
 * 19-Nov-2010 : Version 0.0.1 (NM);
 */

package com.istic.mmm.kava20.diagramView;

import java.util.Map;

import com.istic.mmm.kava20.R;
import com.istic.mmm.kava20.diagramView.DemoView;

import org.afree.chart.ChartFactory;
import org.afree.chart.AFreeChart;
import org.afree.chart.LegendItem;
import org.afree.chart.LegendItemSource;
import org.afree.chart.plot.PiePlot;
import org.afree.chart.title.LegendTitle;
import org.afree.chart.title.TextTitle;
import org.afree.data.general.DefaultPieDataset;
import org.afree.data.general.PieDataset;
import org.afree.graphics.geom.Font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * PieChartDemo01View
 */
public class DiagramEtatVinView extends DemoView {
	private Map<String,Double> mapEtatVinPercentage;
	
    /**
     * constructor
     * @param context
     */
    public DiagramEtatVinView(Context context,Map<String,Double> pMapEtatVin,String pTitle) {
        super(context);

        final PieDataset dataset = createDataset(pMapEtatVin);
        final AFreeChart chart = createChart(dataset,pTitle);
        setChart(chart);
    }

    /**
     * Creates a sample dataset.
     * @return a sample dataset.
     */
    private static PieDataset createDataset(Map<String,Double> pMapEtatVin) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        for (String etatVin : pMapEtatVin.keySet()) {
        	dataset.setValue(etatVin, pMapEtatVin.get(etatVin));
		}
        return dataset;
    }

    /**
     * Creates a chart.
     * @param dataset the dataset.
     * @return a chart.
     */
    private static AFreeChart createChart(PieDataset dataset,String pTitle) {

        AFreeChart chart = ChartFactory.createPieChart(
        		pTitle, // chart title
                dataset, // data
                true, // include legend
                true,
                false);
        TextTitle title = chart.getTitle();
        title.setToolTipText("A title tooltip!");

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Typeface.NORMAL, 20));
        plot.setNoDataMessage("No data available");
        plot.setCircular(true);
        plot.setLabelGap(0.01);
        return chart;

    }

	public Map<String, Double> getMapEtatVinPercentage() {
		return mapEtatVinPercentage;
	}

	public void setMapEtatVinPercentage(Map<String, Double> mapEtatVinPercentage) {
		this.mapEtatVinPercentage = mapEtatVinPercentage;
	}
}
