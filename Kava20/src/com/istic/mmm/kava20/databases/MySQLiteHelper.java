package com.istic.mmm.kava20.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_VIN = "table_vin";
	public static final String COLUMN_VIN_ID = "_idVin";
	public static final String COLUMN_VIN_COMMENT = "_comment";
	public static final String COLUMN_VIN_COULEUR = "_couleur";
	public static final String COLUMN_VIN_ANNEE = "_annee";	
	public static final String COLUMN_VIN_NBBOUTEILLES = "_nbBouteilles";
	public static final String COLUMN_VIN_NBBOUTEILLEBUES = "_nbBouteilleBues";
	public static final String COLUMN_VIN_ETAT = "_etat";
	public static final String COLUMN_VIN_ANNEEABOIRE = "_anneeABoire";
	public static final String COLUMN_VIN_NOTE = "_note";
	public static final String COLUMN_VIN_PHOTOLIEN = "_photoLien";
	
	public static final String COLUMN_VIN_PROVENANCE_ID = "_idVinProv";
	
	public static final String TABLE_PROVENANCE = "table_provenance";
	public static final String COLUMN_PROV_ID = "_idProvenance";
	public static final String COLUMN_PROV_AOC = "_aoc";
	public static final String COLUMN_PROV_DOMAINE = "_domaine";
	public static final String COLUMN_PROV_REGION = "_region";
	public static final String COLUMN_PROV_CEPAGE = "_cepage";
	public static final String COLUMN_PROV_TERROIR = "_terroir";


	private static final String DATABASE_NAME = "kava20.db";
	private static final int DATABASE_VERSION = 5;

	private static MySQLiteHelper _mysqlitehelper;
	
	private Context _cxt;
	
	// Commande sql pour la creation de la base de donnees
	private static final String DATABASE_CREATE_VIN = "create table "
			+ TABLE_VIN + "(" 
			+ COLUMN_VIN_ID + " integer primary key autoincrement, " 
			+ COLUMN_VIN_COMMENT + " text, "
			+ COLUMN_VIN_COULEUR + " text not null, "
			+ COLUMN_VIN_ANNEE + " integer, "
			+ COLUMN_VIN_NBBOUTEILLES + " integer, "
			+ COLUMN_VIN_NBBOUTEILLEBUES + " integer, "
			+ COLUMN_VIN_ETAT + " integer, "
			+ COLUMN_VIN_ANNEEABOIRE + " integer, "
			+ COLUMN_VIN_NOTE + " float, "
			+ COLUMN_VIN_PHOTOLIEN + " text, "
			+ COLUMN_VIN_PROVENANCE_ID + " integer"
			+");";
	
	// Commande sql pour la creation de la base de donnees
	private static final String DATABASE_CREATE_PROVENANCE = "create table "
			+ TABLE_PROVENANCE + "(" 
			+ COLUMN_PROV_ID + " integer primary key autoincrement, "
			+ COLUMN_PROV_AOC + " text not null, "
			+ COLUMN_PROV_DOMAINE + " text, "
			+ COLUMN_PROV_REGION + " text, "
			+ COLUMN_PROV_CEPAGE + " text, "
			+ COLUMN_PROV_TERROIR + " text"
			+ ");";


    public static synchronized MySQLiteHelper getInstance(Context ctx) {
        /** 
         * use the application context as suggested by CommonsWare.
         * this will ensure that you dont accidentally leak an Activitys
         * context (see this article for more information: 
         * http://android-developers.blogspot.nl/2009/01/avoiding-memory-leaks.html)
         */
        if (_mysqlitehelper == null) {
        	_mysqlitehelper = new MySQLiteHelper(ctx.getApplicationContext());
        }
        return _mysqlitehelper;
    }

    /**
     * constructor should be private to prevent direct instantiation.
     * make call to static factory method "getInstance()" instead.
     */
    private MySQLiteHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
        this._cxt = ctx;
    }	
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_VIN);
		database.execSQL(DATABASE_CREATE_PROVENANCE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIN);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROVENANCE);
		onCreate(db);
	}
}

