package com.istic.mmm.kava20.databases;

import java.util.ArrayList;
import java.util.List;

import com.istic.mmm.kava20.model.Provenance;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ProvenanceDataSource {
	// Champs de la base de donnees
	private SQLiteDatabase _database;
	private MySQLiteHelper _dbHelper;
	
	private static final int NUM_COL_ID=0;
	private static final int NUM_COL_AOC=1;
	private static final int NUM_COL_DOMAINE=2;
	private static final int NUM_COL_REGION=3;
	private static final int NUM_COL_CEPAGE=4;
	private static final int NUM_COL_TERROIR=5;
	
	//colonnes de la base de donnees
	private String[] _allColumns = { MySQLiteHelper.COLUMN_PROV_ID,
			MySQLiteHelper.COLUMN_PROV_AOC,
			MySQLiteHelper.COLUMN_PROV_DOMAINE,
			MySQLiteHelper.COLUMN_PROV_REGION,
			MySQLiteHelper.COLUMN_PROV_CEPAGE,
			MySQLiteHelper.COLUMN_PROV_TERROIR};

	public ProvenanceDataSource(Context context) {
		_dbHelper = MySQLiteHelper.getInstance(context);
	}

	public void open() throws SQLException {
		_database = _dbHelper.getWritableDatabase();
	}

	public void close() {
		_dbHelper.close();
	}

	public SQLiteDatabase get_database() {
		return _database;
	}
	/**
	 * permet d'inserer une nouvelle Provenance dans la base de donnees
	 * @param pProvenance objet vin a inserer
	 * @return objet Provenance insere
	 */
	public Provenance createProvenance(Provenance pProvenance) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_PROV_AOC, pProvenance.get_aoc());
		values.put(MySQLiteHelper.COLUMN_PROV_DOMAINE, pProvenance.get_domaine());
		values.put(MySQLiteHelper.COLUMN_PROV_REGION, pProvenance.get_region());
		values.put(MySQLiteHelper.COLUMN_PROV_CEPAGE, pProvenance.get_cepage());
		values.put(MySQLiteHelper.COLUMN_PROV_TERROIR, pProvenance.get_terroir());		
		
		long insertId = _database.insert(MySQLiteHelper.TABLE_PROVENANCE, null,
				values);
		Cursor cursor = _database.query(MySQLiteHelper.TABLE_PROVENANCE,
				_allColumns, MySQLiteHelper.COLUMN_PROV_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		Provenance newProvenance = cursorToProvenance(cursor);
		cursor.close();
		return newProvenance;
	}
	/**
	 * permet de mettre a jour les donnees d'une Provenance dans la base de donnees
	 * @param pId id de la Provenance a mettre a jour
	 * @param pProvenance nouvel objet Provenance
	 * @return le nombre de lignes modifiees de la table 
	 */
	public int updateProvenance(int pId,Provenance pProvenance){
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_PROV_AOC, pProvenance.get_aoc());
		values.put(MySQLiteHelper.COLUMN_PROV_DOMAINE, pProvenance.get_domaine());
		values.put(MySQLiteHelper.COLUMN_PROV_REGION, pProvenance.get_region());
		values.put(MySQLiteHelper.COLUMN_PROV_CEPAGE, pProvenance.get_cepage());
		values.put(MySQLiteHelper.COLUMN_PROV_TERROIR, pProvenance.get_terroir());
		return _database.update(MySQLiteHelper.TABLE_PROVENANCE, values, MySQLiteHelper.COLUMN_PROV_ID + " = " +pId, null);
	}
	/**
	 * permet de supprimer une ligne dans la table TABLE_PROVENANCE
	 * @param pIdProvenance id de la Provenance a supprimer
	 */
	public void deleteProvenance(int pIdProvenance) {
		System.out.println("Provenance deleted with id: " + pIdProvenance);
		_database.delete(MySQLiteHelper.TABLE_PROVENANCE, MySQLiteHelper.COLUMN_PROV_ID
				+ " = " + pIdProvenance, null);
	}
	/**
	 * permet de vider la table TABLE_PROVENANCE
	 */
	public void removeAllProvenances(){
		List<Provenance> listProvenances=getAllProvenances();
		for (Provenance prov : listProvenances) {
			deleteProvenance(prov.get_idProvenance());
		}
		
	}
	/**
	 * permet de recuperer une provenance a partir de son id
	 * @param idProvenance id de la provenance a recuperer
	 * @return objet Provenance 
	 */
	public Provenance getProvenanceById(int idProvenance){
		Provenance provenance = null;
		Cursor cursor = _database.query(MySQLiteHelper.TABLE_PROVENANCE,
				_allColumns, MySQLiteHelper.COLUMN_PROV_ID + " = " +idProvenance, null, null, null, null);
		if(cursor.getCount()>0){
			cursor.moveToFirst();
			provenance = cursorToProvenance(cursor);
		}
		cursor.close();
		return provenance;
	}
	/**
	 * permet de recuperer une provenance a partir de son aoc
	 * @param pAoc aoc de la provenance a recuperer
	 * @return objetProvenance
	 */
	public Provenance getProvenanceByAoc(String pAoc){
		Provenance provenance = null;
		Cursor cursor = _database.query(MySQLiteHelper.TABLE_PROVENANCE,
				_allColumns, MySQLiteHelper.COLUMN_PROV_AOC + " = " +pAoc, null, null, null, null);
		if(cursor.getCount()>0){
			cursor.moveToFirst();
			provenance = cursorToProvenance(cursor);
		}
		cursor.close();
		return provenance;
	}
	/**
	 * permet de recuperer la liste de toutes les Provenances dans la base
	 * @return liste de toutes les Provenances
	 */
	public List<Provenance> getAllProvenances() {
		List<Provenance> listProvenances = new ArrayList<Provenance>();

		Cursor cursor = _database.query(MySQLiteHelper.TABLE_PROVENANCE,
				_allColumns, null, null, null, null, null);

		System.out.println("cursor count = "+ cursor.getCount());
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Provenance prov = cursorToProvenance(cursor);
			listProvenances.add(prov);
			cursor.moveToNext();
		}
		// assurez-vous de la fermeture du curseur
		cursor.close();
		return listProvenances;
	}
	/**
	 * permet de recuperer l'objet Provenance a partir du cursor
	 * @param cursor Cursor
	 * @return objet Provenance
	 */
	private Provenance cursorToProvenance(Cursor cursor) {
//		if(cursor.getCount()==0){
//			return null;
//		}
//		cursor.moveToFirst();
		
		Provenance prov = new Provenance();
		prov.set_idProvenance(cursor.getInt(NUM_COL_ID));
		prov.set_aoc(cursor.getString(NUM_COL_AOC));
		prov.set_domaine(cursor.getString(NUM_COL_DOMAINE));
		prov.set_region(cursor.getString(NUM_COL_REGION));
		prov.set_cepage(cursor.getString(NUM_COL_CEPAGE));
		prov.set_terroir(cursor.getString(NUM_COL_TERROIR));
		
//		cursor.close();
		return prov;
	}
}
