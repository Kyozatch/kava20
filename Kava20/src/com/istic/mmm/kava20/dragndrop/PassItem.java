package com.istic.mmm.kava20.dragndrop;

import android.view.View;
import com.istic.mmm.kava20.itemcustom.ItemListWine;

public class PassItem {

	private View view;
	private ItemListWine item;

	public PassItem(View v, ItemListWine i){
		setView(v);
		setItem(i);
	}

	public ItemListWine getItem() {
		return item;
	}

	public void setItem(ItemListWine item) {
		this.item = item;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}
}


