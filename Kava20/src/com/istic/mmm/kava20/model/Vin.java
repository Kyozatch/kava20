package com.istic.mmm.kava20.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Vin {
	private int _idVin;
	private String _comment;
	private String _couleur;
	private int _annee;
	private int _nbBouteilles;
	private int _nbBouteilleBues;
	private int _etat;
	private int _anneeABoire;
	private float _note;
	private String _photoLien;
	private Provenance _provenance;
	
	private static final Map<Integer, String> _etatVinMap=new HashMap<Integer, String>(){{
		put(0,"non classe");
		put(1,"maturation");
		put(2,"a boire");
		put(3,"a boire tres vite");
	}};
	private static final List<String> _listCouleursVins=new ArrayList<String>(){{
		add("Rouge");
		add("Blanc");
		add("Jaune");
		add("Rosé");		
	}};
	
	
	public int get_idVin() {
		return _idVin;
	}
	public void set_idVin(int _idVin) {
		this._idVin = _idVin;
	}
	public String get_comment() {
		return _comment;
	}
	public void set_comment(String _comment) {
		this._comment = _comment;
	}
	public String get_couleur() {
		return _couleur;
	}
	public void set_couleur(String _couleur) {
		this._couleur = _couleur;
	}
	public int get_annee() {
		return _annee;
	}
	public void set_annee(int _annee) {
		this._annee = _annee;
	}
	public int get_nbBouteilles() {
		return _nbBouteilles;
	}
	public void set_nbBouteilles(int _nbBouteilles) {
		this._nbBouteilles = _nbBouteilles;
	}
	public int get_nbBouteilleBues() {
		return _nbBouteilleBues;
	}
	public void set_nbBouteilleBues(int _nbBouteilleBues) {
		this._nbBouteilleBues = _nbBouteilleBues;
	}
	public int get_etat() {
		return _etat;
	}
	public void set_etat(int _etat) {
		this._etat = _etat;
	}
	public int get_anneeABoire() {
		return _anneeABoire;
	}
	public void set_anneeABoire(int _anneeABoire) {
		this._anneeABoire = _anneeABoire;
	}
	public float get_note() {
		return _note;
	}
	public void set_note(float _note) {
		this._note = _note;
	}
	public String get_photoLien() {
		return _photoLien;
	}
	public void set_photoLien(String _photoLien) {
		this._photoLien = _photoLien;
	}
	public Provenance get_provenance() {
		return _provenance;
	}
	public void set_provenance(Provenance _provenance) {
		this._provenance = _provenance;
	}
	public static Map<Integer, String> getEtatvinmap() {
		return _etatVinMap;
	}
	public static List<String> getListcouleursvins() {
		return _listCouleursVins;
	}
}
