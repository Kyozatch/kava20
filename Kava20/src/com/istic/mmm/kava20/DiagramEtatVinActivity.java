package com.istic.mmm.kava20;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.istic.mmm.kava20.databases.ProvenanceDataSource;
import com.istic.mmm.kava20.databases.VinDataSource;
import com.istic.mmm.kava20.diagramView.DiagramEtatVinView;
import com.istic.mmm.kava20.model.Provenance;
import com.istic.mmm.kava20.model.Vin;
import com.istic.mmm.kava20.utils.CalculUtils;

public class DiagramEtatVinActivity extends Activity{
	private VinDataSource vin_DS;
	private ProvenanceDataSource p_DS;
	private static int _allVinsNb;
	private static int _allVinsRougeNb;
	private static int _allVinsBlancNb;
	private static int _allVinsJauneNb;
	private static int _allVinsRoseNb;
	
	
	private Button _buttonAll; 
	private Button _buttonRed;
	private Button _buttonWhite;
	private Button _buttonYellow;
	private Button _buttonRose;
	private Button _selectedButton;
	
	private DiagramEtatVinView _currentView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.test_profil_vin);
		vin_DS=new VinDataSource(this);
		vin_DS.open();
		
		p_DS=new ProvenanceDataSource(this);
		p_DS.open();
		
//		p_DS.removeAllProvenances();
//		vin_DS.removeAllVins();
//		testinsertData("aoc1", "rouge", 6, 0);
//		testinsertData("aoc2", "blanc", 25, 1);
//		testinsertData("aoc3", "rouge", 16, 2);
//		testinsertData("aoc4", "rose", 20, 3);
		
//		displayAllVins();
		
		//recuperation des pourcentages
		_allVinsNb=countNbVinsByEtatAndCouleur(-1, null);
		Map<String,Double> mapEtatVinAll=fillMapEtat("Tout");
		
		final LinearLayout layout2=new LinearLayout(this);	
		final DiagramEtatVinView diagAllView=new DiagramEtatVinView(this,mapEtatVinAll,(String)getText(R.string.wineBottleState) + " = " +_allVinsNb);
		
		//vue par defaut
		diagAllView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		layout2.addView(diagAllView);
		set_currentView(diagAllView);
		

		_allVinsRougeNb=countNbVinsByEtatAndCouleur(-1, "Rouge");
		Map<String,Double> mapEtatVinRouge=fillMapEtat("Rouge");
		final DiagramEtatVinView diagVinRougeView=new DiagramEtatVinView(this,mapEtatVinRouge,(String)getText(R.string.wineBottleRedState) + " = " +_allVinsRougeNb);
		
		_allVinsBlancNb=countNbVinsByEtatAndCouleur(-1, "Blanc");
		Map<String,Double> mapEtatVinBlanc=fillMapEtat("Blanc");
		final DiagramEtatVinView diagVinBlancView=new DiagramEtatVinView(this,mapEtatVinBlanc,(String)getText(R.string.wineBottleWhiteState) + " = " +_allVinsBlancNb);
		
		_allVinsJauneNb=countNbVinsByEtatAndCouleur(-1, "Jaune");
		Map<String,Double> mapEtatVinJaune=fillMapEtat("Jaune");
		final DiagramEtatVinView diagVinJauneView=new DiagramEtatVinView(this,mapEtatVinJaune,(String)getText(R.string.wineBottleYellowState) + " = " +_allVinsJauneNb);
		
		_allVinsRoseNb=countNbVinsByEtatAndCouleur(-1, "Rosé");
		Map<String,Double> mapEtatVinRose=fillMapEtat("Rosé");
		final DiagramEtatVinView diagVinRoseView=new DiagramEtatVinView(this,mapEtatVinRose,(String)getText(R.string.wineBottleRoseState) + " = " +_allVinsRoseNb);
		
		//Layout principal
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		//Layout contenant les Buttons
		LinearLayout layout1=new LinearLayout(this);
		layout1.setOrientation(LinearLayout.HORIZONTAL);
		layout1.setGravity(Gravity.TOP);
		layout1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		//ajout des Buttons
		_buttonAll=new Button(this);
		_buttonAll.setText(R.string.all);
		_buttonAll.setTextSize(16);
		_buttonAll.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0.2f));
		_buttonAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				layout2.removeView(_currentView);
				layout2.addView(diagAllView);
				set_currentView(diagAllView);
				setSelectedButtonColored(_buttonAll, "all");
			}
		});
		layout1.addView(_buttonAll);
		
		_buttonRed=new Button(this);
		_buttonRed.setText(R.string.redVineChoice);
		_buttonRed.setTextSize(16);
		_buttonRed.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0.2f));
		_buttonRed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				layout2.removeView(_currentView);
				layout2.addView(diagVinRougeView);
				set_currentView(diagVinRougeView);
				setSelectedButtonColored(_buttonRed, "red");
			}
		});
		layout1.addView(_buttonRed);
		
		_buttonWhite=new Button(this);
		_buttonWhite.setText(R.string.whiteVineChoice);
		_buttonWhite.setTextSize(16);
		_buttonWhite.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0.2f));
		_buttonWhite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				layout2.removeView(_currentView);
				layout2.addView(diagVinBlancView);
				set_currentView(diagVinBlancView);
				setSelectedButtonColored(_buttonWhite, "white");
			}
		});
		layout1.addView(_buttonWhite);
		
		_buttonYellow=new Button(this);
		_buttonYellow.setText(R.string.yellowVineChoice);
		_buttonYellow.setTextSize(16);
		_buttonYellow.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0.2f));
		_buttonYellow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				layout2.removeView(_currentView);
				layout2.addView(diagVinJauneView);
				set_currentView(diagVinJauneView);
				setSelectedButtonColored(_buttonYellow, "yellow");
			}
		});
		layout1.addView(_buttonYellow);
		
		_buttonRose=new Button(this);
		_buttonRose.setText(R.string.pinkVineChoice);
		_buttonRose.setTextSize(16);
		_buttonRose.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0.2f));
		_buttonRose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				layout2.removeView(_currentView);
				layout2.addView(diagVinRoseView);
				set_currentView(diagVinRoseView);
				setSelectedButtonColored(_buttonRose, "rose");
			}
		});
		layout1.addView(_buttonRose);
		mainLayout.addView(layout1);
		mainLayout.addView(layout2);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		initializeButtonsColor();
		setSelectedButtonColored(_buttonAll, "all");
		setContentView(mainLayout);
	}
	public void displayAllVins(){
		List<Vin> listVins=vin_DS.getAllVins();
		int compteurV=0;
		System.out.println("Vins:");
		for (Vin vin : listVins) {
			compteurV++;
			System.out.println(compteurV + " "+ vin.get_idVin()+" "+ vin.get_couleur()+ " "+ vin.get_provenance().get_aoc()+" "+vin.get_etat());
		}
		System.out.println();
	
	}
	public void testinsertData(String aoc,String couleur, int nbBouteilles, int etat){
		Provenance prov=new Provenance();
		prov.set_aoc(aoc);
		Provenance provInserted=p_DS.createProvenance(prov);
		
		Vin vin=new Vin();
		vin.set_couleur(couleur);
		vin.set_nbBouteilles(nbBouteilles);
		vin.set_etat(etat);
		vin.set_provenance(provInserted);
		vin_DS.createVin(vin);
		
	}
	/**
	 * permet de remplir un map contenant l'etat d'un vin et le pourcentage 
	 * @return Map avec l'etat comme cle et le pourcentage comme valeur
	 */
	public Map<String,Double> fillMapEtat(String pCouleur){
		Map<String,Double> mapEtatVin=new HashMap<String, Double>();
		Map<Integer, String> etatVinMap=Vin.getEtatvinmap();
		int totalVinsNb;
		
//		switch (pCouleur) {
//		case "Rouge":
//			totalVinsNb=_allVinsRougeNb;
//			break;
//		case "Blanc":
//			totalVinsNb=_allVinsRougeNb;
//			break;
//		case "Jaune":
//			totalVinsNb=_allVinsRougeNb;
//			break;
//			
//		case "Rosé":
//			totalVinsNb=_allVinsRougeNb;
//			break;
//
//		default:
//			totalVinsNb=_allVinsNb;
//			pCouleur=null;
//			break;
//		}
		
		if(pCouleur.equalsIgnoreCase("Rouge")){
			totalVinsNb=_allVinsRougeNb;
			System.out.println("total rouge " + totalVinsNb);
		}
		else if(pCouleur.equalsIgnoreCase("Blanc")){
			totalVinsNb=_allVinsBlancNb;
		}
		else if(pCouleur.equalsIgnoreCase("Jaune")){
			totalVinsNb=_allVinsJauneNb;
		}
		else if(pCouleur.equalsIgnoreCase("Rosé")){
			totalVinsNb=_allVinsRoseNb;
		}
		else{
			totalVinsNb=_allVinsNb;
			pCouleur=null;
		}
		for (int etatInt : etatVinMap.keySet()) {
			int vinNb=countNbVinsByEtatAndCouleur(etatInt, pCouleur);
			double percentage=CalculUtils.getPercentage(vinNb, totalVinsNb);
			mapEtatVin.put(etatVinMap.get(etatInt) + " = " + vinNb, percentage);
		}
		return mapEtatVin;
	}
	/**
	 * permet de compter le nombre de bouteilles de Vins selon l'etat
	 * @param pEtat etat du vin. valeur a -1 si on veut prendre tous les vins sans considerer l'etat
	 * @param pCouleur couleur du Vin. Mettre a null si on ne specifie pas la couleur
	 * @return le nombre de vins selon l'etat
	 */
	public int countNbVinsByEtatAndCouleur(int pEtat,String pCouleur){
		int nbVins=0;
		List<Vin> listVinsByEtat=new ArrayList<Vin>();
		if(pEtat==-1 && pCouleur==null){
			listVinsByEtat=vin_DS.getAllVins();
		}
		if(pEtat>=0 && pCouleur==null){
			listVinsByEtat=vin_DS.getListVinByEtat(pEtat);
		}
		if(pCouleur!=null){
			listVinsByEtat=vin_DS.getListVinByEtatAndCouleur(pEtat, pCouleur);
		}
		for (Vin vin : listVinsByEtat) {
			nbVins=nbVins+vin.get_nbBouteilles();
		}
		return nbVins;
	}
	public void set_currentView(DiagramEtatVinView _currentView) {
		this._currentView = _currentView;
	}
	public void set_selectedButton(Button _selectedButton) {
		this._selectedButton = _selectedButton;
	}
	/**
	 * permet de colorer le bouton selectionne
	 * @param pButton Button a colorer
	 * @param pColor couleur du Button
	 */
	public void setSelectedButtonColored(Button pButton,String pColor){
		if(_selectedButton!=null){
			_selectedButton.setBackgroundResource(0);
		}
		if(pColor.equalsIgnoreCase("red")){
			pButton.setBackgroundColor(getResources().getColor(R.color.redButton));
		}else if(pColor.equalsIgnoreCase("white")){
			pButton.setBackgroundColor(getResources().getColor(R.color.whiteButton));
		}else if(pColor.equalsIgnoreCase("yellow")){
			pButton.setBackgroundColor(getResources().getColor(R.color.yellowButton));
		}else if(pColor.equalsIgnoreCase("white")){
			pButton.setBackgroundColor(getResources().getColor(R.color.whiteButton));
		}else if(pColor.equalsIgnoreCase("rose")){
			pButton.setBackgroundColor(getResources().getColor(R.color.roseButton));
		}else{
			pButton.setBackgroundColor(getResources().getColor(R.color.selectbutton));
		}
		
//		switch (pColor) {
//		case "red":
//			pButton.setBackgroundColor(getResources().getColor(R.color.redButton));
//			break;
//		case "white":
//			pButton.setBackgroundColor(getResources().getColor(R.color.whiteButton));
//			break;
//		case "yellow":
//			pButton.setBackgroundColor(getResources().getColor(R.color.yellowButton));
//			break;
//		case "rose":
//			pButton.setBackgroundColor(getResources().getColor(R.color.roseButton));
//			break;
//		default:
//			pButton.setBackgroundColor(getResources().getColor(R.color.selectbutton));
//			break;
//		}
		set_selectedButton(pButton);
	}
	/**
	 * pour initialiser la couleur des Buttons
	 */
	public void initializeButtonsColor(){
		_buttonRed.setBackgroundResource(0);
		_buttonWhite.setBackgroundResource(0);
		_buttonYellow.setBackgroundResource(0);
		_buttonRose.setBackgroundResource(0);
	}
}