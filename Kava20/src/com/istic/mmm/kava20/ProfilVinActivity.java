package com.istic.mmm.kava20;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.istic.mmm.kava20.databases.ProvenanceDataSource;
import com.istic.mmm.kava20.databases.VinDataSource;
import com.istic.mmm.kava20.model.Provenance;
import com.istic.mmm.kava20.model.Vin;

public class ProfilVinActivity extends Activity{

	public final static String ID_WINE = "com.istic.mmm.kava20.ID_WINE";

	private Uri imageUri;
	private ContentValues values;

	private Vin vin;
	private Provenance provenance;

	private int typeAction;

	private int nbBottle;

	private EditText aocTxT;
	private EditText yearTxT;
	private RatingBar ratingbar;
	private EditText regionTxT;
	private EditText domaineTxT;
	private EditText commentTxT;
	private RadioGroup colorGroup;
	private TextView nbBottleTxT;
	private ImageView vinePicture;

	private VinDataSource v_DS;
	private ProvenanceDataSource p_DS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profil_vin);

		typeAction = 0;

		Intent intent = getIntent();
		
		try {
			String id_wine = (String) intent.getSerializableExtra(ID_WINE);
			typeAction = Integer.parseInt(id_wine);
		} catch (Exception e) {

		}

		// ---- INITIATE ----

		aocTxT   = (EditText)findViewById(R.id.aocTxT);
		yearTxT = (EditText)findViewById(R.id.yearTxT);
		ratingbar = (RatingBar) findViewById(R.id.ratingBar);
		colorGroup = (RadioGroup) findViewById(R.id.radioGroupColor);
		domaineTxT = (EditText)findViewById(R.id.domaineTxT);
		regionTxT = (EditText) findViewById(R.id.regionTxT);
		commentTxT   = (EditText)findViewById(R.id.editTextComment);
		nbBottleTxT = (TextView)findViewById(R.id.editTextNbBottle);
		vinePicture = (ImageView)findViewById(R.id.imageView1);

		vinePicture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				open();
			}
		});

		// ------ DB CONNECTION  ------

		v_DS = new VinDataSource(this);
		v_DS.open();

		p_DS = new ProvenanceDataSource(this);
		p_DS.open();

		initialize(typeAction, intent);		
	}

	public void initialize(int id, Intent intent){

		switch(id){
		// Case for a new Vin
		case 0:

			commentTxT.setText("");
			nbBottle = 1;
			nbBottleTxT.setText(nbBottle+"");
			ratingbar.setRating((float) 0);

			vin = new Vin();
			vin.set_couleur("red");
			vin.set_comment(R.id.editTextComment+"");

			provenance = new Provenance();
			
			// IF YOU SHARE PICTURE FROM GALLERY
			
			if (Intent.ACTION_SEND.equals(intent.getAction())) {   
		        if (intent.getExtras().containsKey(Intent.EXTRA_STREAM)) {
		        	Uri uri = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
					vinePicture.setImageURI(uri);
					vin.set_photoLien(uri.toString());
		        }
		    }

			break;

			// Case for an update
		default :

			vin = v_DS.getVinById(id);
			provenance = vin.get_provenance();

			// Champs � pr�-remplir
			// VIN :

			yearTxT.setText(vin.get_annee()+"");
			commentTxT.setText(vin.get_comment());

			if(vin.get_couleur().equals("Jaune")){
				RadioButton rdby = (RadioButton) findViewById(R.id.radiobutton_yellow);
				rdby.setChecked(true);
			}
			else if(vin.get_couleur().equals("Ros�")){
				RadioButton rdbp = (RadioButton) findViewById(R.id.radiobutton_pink);
				rdbp.setChecked(true);
			}
			else if(vin.get_couleur().equals("Blanc")){
				RadioButton rdbw = (RadioButton) findViewById(R.id.radiobutton_white);
				rdbw.setChecked(true);
			}
			else{
				RadioButton rdbr = (RadioButton) findViewById(R.id.radiobutton_red);
				rdbr.setChecked(true);
			}

			ratingbar.setRating(vin.get_note());

			if(vin.get_photoLien() != null){

				Uri uripic = null;
				uripic = Uri.parse(vin.get_photoLien());
				//vinePicture.setImageURI(uripic);

				Bitmap thumbnail;
				try {
					thumbnail = MediaStore.Images.Media.getBitmap(
							getContentResolver(), uripic);
					Bitmap lightBitmap = convert(thumbnail, Bitmap.Config.RGB_565);
					vinePicture.setImageBitmap(lightBitmap);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			nbBottleTxT.setText(vin.get_nbBouteilles()+"");
			nbBottle = vin.get_nbBouteilles();

			// Provenance :
			aocTxT.setText(provenance.get_aoc());
			regionTxT.setText(provenance.get_region());
			domaineTxT.setText(provenance.get_domaine());

			break;
		}
	}

	/*
	 * This function is called when user switch the device's orientation
	 * Reset with current params
	 */
	public void onConfigurationChanged(Configuration newConfig, int requestCode, int resultCode, Intent data) {
		super.onConfigurationChanged(newConfig);
		super.onActivityResult(requestCode, resultCode, data);

		SaveAndSetVinePicture(data);
	}

	// Open Camera and take picture
	public void open(){
		values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, "New Vine");
		values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
		imageUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(intent, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}    

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

		case 0:
			if (resultCode == Activity.RESULT_OK) {
				try {
					System.out.println(imageUri);

					Bitmap thumbnail = MediaStore.Images.Media.getBitmap(
							getContentResolver(), imageUri);
					Bitmap lightBitmap = convert(thumbnail, Bitmap.Config.RGB_565);

					vinePicture.setImageBitmap(lightBitmap);
					vin.set_photoLien(imageUri.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		//SaveAndSetVinePicture(data);
	}

	private Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
		Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
		Canvas canvas = new Canvas(convertedBitmap);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		canvas.drawBitmap(bitmap, 0, 0, paint);
		return convertedBitmap;
	}

	// Function called to save the new vinePicture and set new PathToFile
	public void SaveAndSetVinePicture(Intent data){
		if(data != null){


			Bundle intentData = data.getExtras();
			Bitmap vineBP = (Bitmap) intentData.get("data");

			Uri uri = data.getData();

			vinePicture.setImageBitmap(vineBP);
			vin.set_photoLien(uri.toString());   

			//System.out.println(uri);

		}
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	}

	public void addNbBottle(View view){
		nbBottle++;
		nbBottleTxT.setText(nbBottle+"");
	}

	public void removeNbBottle(View view){
		if(nbBottle > 0){
			nbBottle--;
			nbBottleTxT.setText(nbBottle+"");
		}
	}

	public void saveOrUpdateVine(View view){

		boolean isNothingEmpty = true;
		String emptyField = "Veuillez remplir le champ svp";

		// year not empty
		int yearInt = 1;
		try{
			yearInt = Integer.parseInt(yearTxT.getText().toString());
			yearTxT.setError(null);
		} catch (NumberFormatException e) {
			isNothingEmpty = false;
			yearTxT.setError(emptyField);
		}

		// AOC not empty
		if(aocTxT.getText().toString().matches("")){
			isNothingEmpty = false;
			aocTxT.setError(emptyField);
		}else{aocTxT.setError(null);}

		// If isNothingEmpty -> set in DB

		if(isNothingEmpty){

			// --- PROVENANCE ---

			//p_DS.removeAllProvenances();

			provenance.set_aoc(aocTxT.getText().toString());
			provenance.set_domaine(domaineTxT.getText().toString());
			provenance.set_region(regionTxT.getText().toString());

			Provenance ptemp = p_DS.createProvenance(provenance);

			System.out.println(" ----------- VARIABLE ----------");
			System.out.println("AOC : "+provenance.get_aoc());
			System.out.println("DOMAINE : "+provenance.get_domaine());
			System.out.println("REGION : "+provenance.get_region());

			List<Provenance> listProvenance = p_DS.getAllProvenances();

			for (Provenance provenance : listProvenance) {
				System.out.println(" ----------- BDD ----------");
				System.out.println("ID : "+provenance.get_idProvenance());
				System.out.println("AOC : "+provenance.get_aoc());
				System.out.println("DOMAINE : "+provenance.get_domaine());
				System.out.println("REGION : "+provenance.get_region());
			}

			// --- VIN ----

			vin.set_annee(yearInt);
			vin.set_comment(commentTxT.getText().toString());
			vin.set_nbBouteilles(nbBottle);
			vin.set_provenance(ptemp);
			vin.set_note(ratingbar.getRating());

			int checkedColorId = colorGroup.getCheckedRadioButtonId();
			RadioButton checkedRadioColor = (RadioButton) findViewById(checkedColorId);
			vin.set_couleur(checkedRadioColor.getText().toString());

			//v_DS.removeAllVins();

			System.out.println("------- VARIABLE -----------");
			System.out.println("Comment : "+vin.get_comment());
			System.out.println("Annee : "+vin.get_annee());
			System.out.println("Color : "+vin.get_couleur());
			System.out.println("NbBouteille : "+vin.get_nbBouteilles());
			System.out.println("Rate : "+vin.get_note());

			switch(typeAction){
			case 0:
				v_DS.createVin(vin);
				break;
			default:
				v_DS.updateVin(vin.get_idVin(), vin);
				break;
			}

			List<Vin> listVin = v_DS.getAllVins();

			for (Vin vin : listVin) {
				System.out.println(" ----------- BDD ----------");
				System.out.println("ID : "+vin.get_idVin());
				System.out.println("COmment : "+vin.get_comment());
				System.out.println("Annee : "+vin.get_annee());
				System.out.println("Color : "+vin.get_couleur());
				System.out.println("NbBouteille : "+vin.get_nbBouteilles());
				System.out.println("Rate : "+vin.get_note());
			}

			Intent intent = new Intent(view.getContext(), StartActivity.class);
			startActivity(intent);
		}
	}
}
