package com.istic.mmm.kava20.databases;

import java.util.ArrayList;
import java.util.List;

import com.istic.mmm.kava20.model.Provenance;
import com.istic.mmm.kava20.model.Vin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

public class VinDataSource {
	// Champs de la base de donnees
	private SQLiteDatabase _database;
	private MySQLiteHelper _dbHelper;
	
	private static final int NUM_COL_ID_V=0;
	private static final int NUM_COL_COMMENT=1;
	private static final int NUM_COL_COULEUR=2;
	private static final int NUM_COL_ANNEE=3;
	private static final int NUM_COL_NBBOUTEILLES=4;
	private static final int NUM_COL_NBBOUTEILLESBUES=5;
	private static final int NUM_COL_ETAT=6;
	private static final int NUM_COL_ANNEEABOIRE=7;
	private static final int NUM_COL_NOTE=8;
	private static final int NUM_COL_PHOTOLIEN=9;
//	private static final int NUM_COL_PROVENANCE_ID=10;
	
	private static final int NUM_COL_ID_P=10;
	private static final int NUM_COL_AOC=11;
	private static final int NUM_COL_DOMAINE=12;
	private static final int NUM_COL_REGION=13;
	private static final int NUM_COL_CEPAGE=14;
	private static final int NUM_COL_TERROIR=15;
	
	//colonnes de la base de donnees
	private String[] _allColumns = { 
			MySQLiteHelper.COLUMN_VIN_ID,
			MySQLiteHelper.COLUMN_VIN_COMMENT,
			MySQLiteHelper.COLUMN_VIN_COULEUR,
			MySQLiteHelper.COLUMN_VIN_ANNEE,
			MySQLiteHelper.COLUMN_VIN_NBBOUTEILLES,
			MySQLiteHelper.COLUMN_VIN_NBBOUTEILLEBUES,
			MySQLiteHelper.COLUMN_VIN_ETAT,
			MySQLiteHelper.COLUMN_VIN_ANNEEABOIRE,
			MySQLiteHelper.COLUMN_VIN_NOTE,
			MySQLiteHelper.COLUMN_VIN_PHOTOLIEN/*,
			MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID*/,
			MySQLiteHelper.COLUMN_PROV_ID,
			MySQLiteHelper.COLUMN_PROV_AOC,
			MySQLiteHelper.COLUMN_PROV_DOMAINE,
			MySQLiteHelper.COLUMN_PROV_REGION,
			MySQLiteHelper.COLUMN_PROV_CEPAGE,
			MySQLiteHelper.COLUMN_PROV_TERROIR};

	public VinDataSource(Context context) {
		_dbHelper = MySQLiteHelper.getInstance(context);
	}

	public void open() throws SQLException {
		_database = _dbHelper.getWritableDatabase();
	}

	public void close() {
		_dbHelper.close();
	}

	public SQLiteDatabase get_database() {
		return _database;
	}
	/**
	 * permet d'inserer un nouveau vin dans la base de donnees
	 * @param pVin objet vin a inserer possedant une provenance
	 * @return objet Vin insere
	 */
	public Vin createVin(Vin pVin) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_VIN_COMMENT, pVin.get_comment());
		values.put(MySQLiteHelper.COLUMN_VIN_COULEUR, pVin.get_couleur());
		values.put(MySQLiteHelper.COLUMN_VIN_ANNEE, pVin.get_annee());
		values.put(MySQLiteHelper.COLUMN_VIN_NBBOUTEILLES, pVin.get_nbBouteilles());
		values.put(MySQLiteHelper.COLUMN_VIN_NBBOUTEILLEBUES, pVin.get_nbBouteilleBues());
		values.put(MySQLiteHelper.COLUMN_VIN_ETAT, pVin.get_etat());
		values.put(MySQLiteHelper.COLUMN_VIN_ANNEEABOIRE, pVin.get_anneeABoire());
		values.put(MySQLiteHelper.COLUMN_VIN_NOTE, pVin.get_note());
		values.put(MySQLiteHelper.COLUMN_VIN_PHOTOLIEN, pVin.get_photoLien());
		values.put(MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID, pVin.get_provenance().get_idProvenance());
	
		//insere les valeurs dans la table et recupere l'id
		long insertId = _database.insert(MySQLiteHelper.TABLE_VIN, null,values);
		
		//constrution requete pour recuperer le vin insere avec une jointure avec Provenance
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_VIN_ID + " = " + insertId, null, null, null,null);
 
		cursor.moveToFirst();
		Vin newVin = cursorToVin(cursor);
		
		//fermeture cursor
		cursor.close();
		
		return newVin;
	}
	/**
	 * permet de mettre a jour les donnees d'un Vin dans la base de donnees
	 * @param pId id du vin a mettre a jour
	 * @param pVin nouvel objet Vin
	 * @return le nombre de lignes modifiees de la table 
	 */
	public int updateVin(int pId,Vin pVin){
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_VIN_COMMENT, pVin.get_comment());
		values.put(MySQLiteHelper.COLUMN_VIN_COULEUR, pVin.get_couleur());
		values.put(MySQLiteHelper.COLUMN_VIN_ANNEE, pVin.get_annee());
		values.put(MySQLiteHelper.COLUMN_VIN_NBBOUTEILLES, pVin.get_nbBouteilles());
		values.put(MySQLiteHelper.COLUMN_VIN_NBBOUTEILLEBUES, pVin.get_nbBouteilleBues());
		values.put(MySQLiteHelper.COLUMN_VIN_ETAT, pVin.get_etat());
		values.put(MySQLiteHelper.COLUMN_VIN_ANNEEABOIRE, pVin.get_anneeABoire());
		values.put(MySQLiteHelper.COLUMN_VIN_NOTE, pVin.get_note());
		values.put(MySQLiteHelper.COLUMN_VIN_PHOTOLIEN, pVin.get_photoLien());
		values.put(MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID, pVin.get_provenance().get_idProvenance());
		return _database.update(MySQLiteHelper.TABLE_VIN, values, MySQLiteHelper.COLUMN_VIN_ID + " = " +pId, null);
	}
	/**
	 * permet de supprimer une ligne dans la table TABLE_VIN
	 * @param pIdVin id du vin a supprimer
	 */
	public void deleteVin(int pIdVin) {
//		System.out.println("Vin deleted with id: " + pIdVin);
		_database.delete(MySQLiteHelper.TABLE_VIN, MySQLiteHelper.COLUMN_VIN_ID + " = " + pIdVin, null);
	}
	/**
	 * permet de vider la table TABLE_VIN
	 */
	public void removeAllVins(){
		List<Vin> listVins=getAllVins();
		for (Vin vin : listVins) {
			deleteVin(vin.get_idVin());
		}	
	}
	/**
	 * permet de recuperer un vin a partir de son id
	 * @param idVin id du vin a recuperer
	 * @return objet Vin 
	 */
	public Vin getVinById(int idVin){
		Vin vin = null;	
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_VIN_ID + " = " +idVin, null, null, null,null);
 
		//il faut verifier si le vin avec l'id specifie existe dans la base ou pas
		if(cursor.getCount()>0){
			cursor.moveToFirst();
			vin = cursorToVin(cursor);
		}
		cursor.close();
		return vin;
	}
	/**
	 * permet de recuperer une liste de vins selon des termes de l'aoc
	 * @param pAocSearch aoc du vin en String
	 * @return liste de Vins
	 */
	public List<Vin> searchVinByAocNotExacTWords(String pAocSearch){
		List<Vin> listVins = new ArrayList<Vin>();
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_PROV_AOC + " = %" +pAocSearch+"%", null, null, null,null);
 
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Vin vin = cursorToVin(cursor);
			listVins.add(vin);
			cursor.moveToNext();
		}
		// fermeture cursor
		cursor.close();
		return listVins;
		
	}
	/**
	 * recuperer une liste d'objet Vin a partir de son etat
	 * @param pEtatVin etat du vin (en integer)
	 * @return liste de Vins
	 */
	public List<Vin> getListVinByEtat(int pEtatVin){
		List<Vin> listVins = new ArrayList<Vin>();

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_VIN_ETAT + " = " +pEtatVin, null, null, null,null);
 
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Vin vin = cursorToVin(cursor);
			listVins.add(vin);
			cursor.moveToNext();
		}
		// fermeture cursor
		cursor.close();
		return listVins;
	}
	/**
	 * permet de recuperer une liste d'objet Vin a partir de son etat et de sa couleur
	 * @param pEtatVin etat du vin (en integer). Mettre a -1 si on ne veut pas specifier l'etat du vin
	 * @param pCouleur couleur du vin
	 * @return liste de Vins
	 */
	public List<Vin> getListVinByEtatAndCouleur(int pEtatVin,String pCouleur){
		List<Vin> listVins = new ArrayList<Vin>();

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));

		Cursor cursor; 
		
		if(pEtatVin>=0){
			cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_VIN_ETAT + " = " +pEtatVin + " and " +
					MySQLiteHelper.COLUMN_VIN_COULEUR + " = '" + pCouleur + "'", null, null, null,null);
		}else{
			cursor=queryBuilder.query(_database, _allColumns, MySQLiteHelper.COLUMN_VIN_COULEUR + " = '" + pCouleur +"'", null, null, null,null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Vin vin = cursorToVin(cursor);
			listVins.add(vin);
			cursor.moveToNext();
		}
		// fermeture cursor
		cursor.close();
		return listVins;
	}
	
	/**
	 * permet de recuperer la liste de tous les vins dans la base
	 * @return liste de tous les vins
	 */
	public List<Vin> getAllVins() {
		List<Vin> listVins = new ArrayList<Vin>();

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, null, null, null, null,null);
 
//		System.out.println("cursor count = "+ cursor.getCount());
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Vin vin = cursorToVin(cursor);
			listVins.add(vin);
			cursor.moveToNext();
		}
		// assurez-vous de la fermeture du curseur
		cursor.close();
		return listVins;
	}
	
	/**
	 * permet de recuperer la liste de vins dependant s'il y les bouteilles ont ete bues ou pas
	 * @param pBoolAvailableOrNot true si on veut recuperer les bouteilles de vins bues dispo et false sinon
	 * @return la liste de vins
	 */
	public List<Vin> getListVinsBus(Boolean pBoolAvailableOrNot) {
		List<Vin> listVinsBues = new ArrayList<Vin>();
		
		List<Vin> listVinsNotBues=new ArrayList<Vin>();

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		queryBuilder
		.setTables(MySQLiteHelper.TABLE_VIN
				+ " INNER JOIN "
				+ MySQLiteHelper.TABLE_PROVENANCE
				+ " ON "
				+ MySQLiteHelper.COLUMN_VIN_PROVENANCE_ID
				+ " = "
				+ (MySQLiteHelper.TABLE_PROVENANCE + "." + MySQLiteHelper.COLUMN_PROV_ID));


		Cursor cursor=queryBuilder.query(_database, _allColumns, null, null, null, null,null);
 
//		System.out.println("cursor count = "+ cursor.getCount());
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Vin vin = cursorToVin(cursor);
			if(!(vin.get_nbBouteilleBues()>0)){
				listVinsNotBues.add(vin);
			}else{
				listVinsBues.add(vin);
			}
			cursor.moveToNext();
		}
		// assurez-vous de la fermeture du curseur
		cursor.close();
		
		if(pBoolAvailableOrNot){
			return listVinsBues;
		}else{
			return listVinsNotBues;
		}
	}
	/**
	 * permet de recuperer l'objet vin a partir du cursor
	 * @param cursor Cursor
	 * @return objet Vin
	 */
	private Vin cursorToVin(Cursor cursor) {
//		if(cursor.getCount()==0){
//			return null;
//		}
//		cursor.moveToFirst();

		Vin vin = new Vin();
		vin.set_idVin(cursor.getInt(NUM_COL_ID_V));
		vin.set_comment(cursor.getString(NUM_COL_COMMENT));
		vin.set_couleur(cursor.getString(NUM_COL_COULEUR));
		vin.set_annee(cursor.getInt(NUM_COL_ANNEE));
		vin.set_nbBouteilles(cursor.getInt(NUM_COL_NBBOUTEILLES));
		vin.set_nbBouteilleBues(cursor.getInt(NUM_COL_NBBOUTEILLESBUES));
		vin.set_etat(cursor.getInt(NUM_COL_ETAT));
		vin.set_anneeABoire(cursor.getInt(NUM_COL_ANNEEABOIRE));
		vin.set_note(cursor.getFloat(NUM_COL_NOTE));
		vin.set_photoLien(cursor.getString(NUM_COL_PHOTOLIEN));

		Provenance prov=new Provenance();
		prov.set_idProvenance(cursor.getInt(NUM_COL_ID_P));
		prov.set_aoc(cursor.getString(NUM_COL_AOC));
		prov.set_domaine(cursor.getString(NUM_COL_DOMAINE));
		prov.set_region(cursor.getString(NUM_COL_REGION));
		prov.set_cepage(cursor.getString(NUM_COL_CEPAGE));
		prov.set_terroir(cursor.getString(NUM_COL_TERROIR));

		vin.set_provenance(prov);

//		cursor.close();

		return vin;
	}
}
